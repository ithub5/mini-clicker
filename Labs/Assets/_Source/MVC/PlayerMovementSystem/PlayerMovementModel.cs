using System;
using MVC.PlayerInputSystem;
using UnityEngine.InputSystem;

namespace MVC.PlayerMovementSystem
{
    public class PlayerMovementModel
    {
        private MainInputAction.BasicControlsActions _basicControls;

        private float _speed;
        private float _smoothTime;

        public event Action<InputAction.CallbackContext> OnMoveInput;
        
        public float Speed { get => _speed; }
        public float SmoothTime{ get => _smoothTime; }

        public PlayerMovementModel(MainInputAction.BasicControlsActions basicControls, float speed, float smoothTime)
        {
            _basicControls = basicControls;
            _speed = speed;
            _smoothTime = smoothTime;
            
            Bind();
        }

        private void Bind()
        {
            _basicControls.movement.started += InvokeMove;
            _basicControls.movement.performed += InvokeMove;
            _basicControls.movement.canceled += InvokeMove;
        }

        private void Expose()
        {
            _basicControls.movement.started -= InvokeMove;
            _basicControls.movement.performed -= InvokeMove;
            _basicControls.movement.canceled -= InvokeMove;
        }

        private void InvokeMove(InputAction.CallbackContext ctx) =>
            OnMoveInput?.Invoke(ctx);
    }
}