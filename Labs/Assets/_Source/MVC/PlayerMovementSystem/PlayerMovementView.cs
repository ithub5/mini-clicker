using TMPro;
using UnityEngine;

namespace MVC.PlayerMovementSystem
{
    public class PlayerMovementView : MonoBehaviour
    {
        private TextMeshProUGUI _moveInputText;

        public void Init(TextMeshProUGUI moveInputText)
        {
            _moveInputText = moveInputText;
        }
        
        public void UpdateMoveInput(Vector2 moveInput)
        {
            _moveInputText.text = moveInput.ToString();
        }
    }
}