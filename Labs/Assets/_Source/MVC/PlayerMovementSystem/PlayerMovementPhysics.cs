using System;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

namespace MVC.PlayerMovementSystem
{
    public class PlayerMovementPhysics : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D rb;

        private PlayerMovementModel _movementModel;
        
        private Vector2 _playerInput;
        
        private Vector2 _targetVelocity;
        private Vector2 _currentAcceleration;
        
        private void FixedUpdate()
        {
            if (_playerInput == Vector2.zero && rb)
            {
                
            }
            Move();
        }

        public void Init(PlayerMovementModel movementModel)
        {
            _movementModel = movementModel;
        }

        public void UpdateInput(Vector2 playerInput) =>
            _playerInput = playerInput;

        private void Move()
        {
            _targetVelocity = _playerInput * _movementModel.Speed;
            
            rb.velocity = Vector2.SmoothDamp(
                rb.velocity, _targetVelocity, ref _currentAcceleration, _movementModel.SmoothTime);
        }
    }
}   