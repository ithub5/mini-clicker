using MVC.PlayerHealthSystem;
using MVC.PlayerInputSystem;
using UnityEngine;
using UnityEngine.InputSystem;

namespace MVC.PlayerMovementSystem
{
    public class PlayerMovementController
    {
        private PlayerMovementPhysics _movementPhysics;
        private PlayerMovementModel _movementModel;
        private PlayerMovementView _movementView;

        private PlayerHealthModel _healthModel;
        

        public PlayerMovementController(PlayerMovementPhysics movementPhysics, 
            PlayerMovementModel movementModel, PlayerMovementView movementView, PlayerHealthModel healthModel)
        {
            _movementPhysics = movementPhysics;
            _movementModel = movementModel;
            _movementView = movementView;

            _healthModel = healthModel;

            Bind();
        }

        private void Bind()
        {
            _movementModel.OnMoveInput += TakeMoveInput;

            _healthModel.OnPlayerDeath += Expose;
        }

        private void Expose()
        {
            _movementModel.OnMoveInput -= TakeMoveInput;

            _healthModel.OnPlayerDeath -= Expose;
        }

        private void TakeMoveInput(InputAction.CallbackContext ctx)
        {
            _movementPhysics.UpdateInput(ctx.ReadValue<Vector2>());
            _movementView.UpdateMoveInput(ctx.ReadValue<Vector2>());
        }
    }
}