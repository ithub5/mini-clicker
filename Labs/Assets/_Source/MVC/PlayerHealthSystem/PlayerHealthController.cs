using System.Collections;
using System.Collections.Generic;
using MVC.PlayerMovementSystem;
using UnityEngine;

namespace MVC.PlayerHealthSystem
{
    public class PlayerHealthController
    {
        private PlayerHealthModel _healthModel;
        private PlayerHealthView _healthView;
        
        public PlayerHealthController(PlayerHealthModel healthModel, PlayerHealthView healthView)
        {
            _healthModel = healthModel;
            _healthView = healthView;
            
            Bind();
            _healthModel.Init();
        }

        private void Bind()
        {
            _healthModel.OnHealthChange += OnHealthChanged;
            _healthModel.OnPlayerDeath += OnPlayerDeath;
        }

        private void Expose()
        {
            _healthModel.OnHealthChange -= OnHealthChanged;
            _healthModel.OnPlayerDeath -= OnPlayerDeath;
        }

        private void OnHealthChanged(int newHealth, bool isLess)
        {
            _healthView.DrawHealth(newHealth);
         
            if (!isLess)
            {
                return;
            }
            _healthView.DrawPayerDamaged();
        }
        
        private void OnPlayerDeath()
        {
            _healthView.DrawPlayerDead();
        }
    }
}


