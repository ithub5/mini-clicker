using System;

namespace MVC.PlayerHealthSystem
{
    public class PlayerHealthModel
    {
        private PlayerHealthPhysics _healthPhysics;
        
        private int _health;
        private int _maxHealth;
        
        public event Action<int, bool> OnHealthChange; 
        public event Action OnPlayerDeath;

        public PlayerHealthModel(PlayerHealthPhysics healthPhysics, int maxHealth = 3)
        {
            _healthPhysics = healthPhysics;
            _maxHealth = maxHealth;
            
            Bind();
        }

        public void Init()
        {
            ChangeHealth(_maxHealth);
        }

        private void Bind()
        {
            _healthPhysics.OnEnemyCollision += ChangeHealth;
        }

        private void Expose()
        {
            _healthPhysics.OnEnemyCollision -= ChangeHealth;
        }

        private void ChangeHealth(int healthDelta)
        {
            _health += healthDelta;

            OnHealthChange?.Invoke(_health, healthDelta < 0);
            if (_health <= 0)
            {
                OnPlayerDeath?.Invoke();
            }
        }
    }
}