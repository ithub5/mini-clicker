using System.Collections;
using TMPro;
using UnityEngine;

namespace MVC.PlayerHealthSystem
{
    public class PlayerHealthView : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _renderer;

        [SerializeField] private float colorChangeDuration;
        [SerializeField] private Color damagedColor;
        [SerializeField] private Color deadColor;
        
        private TextMeshProUGUI _healthText;

        private Color _normalColor;

        public void Init(TextMeshProUGUI healthText)
        {
            _healthText = healthText;

            _normalColor = _renderer.color;
        }
        
        public void DrawHealth(int newHealth)
        {
            _healthText.text = newHealth.ToString();
        }

        public void DrawPayerDamaged()
        {
            StopAllCoroutines();
            StartCoroutine(DrawColor(damagedColor));
        }

        public void DrawPlayerDead()
        {
            StopAllCoroutines();
            _renderer.color = deadColor;
        }

        private IEnumerator DrawColor(Color colorToDraw)
        {
            _renderer.color = colorToDraw;
            yield return new WaitForSeconds(colorChangeDuration);
            _renderer.color = _normalColor;
        }
    }
}