using System;
using MVC.EnemySystems;
using MVC.Utils;
using UnityEditor;
using UnityEngine;

namespace MVC.PlayerHealthSystem
{
    public class PlayerHealthPhysics : MonoBehaviour
    {
        [SerializeField] private LayerMask enemyLayerMask;

        public event Action<int> OnEnemyCollision; 

        private void OnCollisionEnter2D(Collision2D col)
        {
            if (!enemyLayerMask.Contains(col.gameObject.layer))
            {
                return;
            }

            int contactDamage = col.gameObject.GetComponent<Enemy>().ContactDamage;
            OnEnemyCollision?.Invoke(-contactDamage);
        }
    }
}