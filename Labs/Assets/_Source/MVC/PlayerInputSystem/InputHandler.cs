using System;
using UnityEngine;

namespace MVC.PlayerInputSystem
{
    public class InputHandler
    {
        private MainInputAction _input;
        
        public MainInputAction.BasicControlsActions BasicControls { get => _input.basicControls; }

        public InputHandler()
        {
            _input = new MainInputAction();
            
            Enable();
        }

        public void Enable()
        {
            _input.Enable();
        }

        public void Disable()
        {
            _input.Disable();
        }
    }
}