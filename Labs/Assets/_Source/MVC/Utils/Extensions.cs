using UnityEngine;

namespace MVC.Utils
{
    public static class Extensions
    {
        public static bool Contains(this LayerMask mask, int layer)
        {
            return mask == (mask | (1 << layer));
        }
    }
}