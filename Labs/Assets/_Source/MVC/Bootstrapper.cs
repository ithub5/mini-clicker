using System;
using UnityEngine;
using MVC.PlayerHealthSystem;
using MVC.PlayerInputSystem;
using MVC.PlayerMovementSystem;
using TMPro;

namespace MVC
{
    public class Bootstrapper : MonoBehaviour
    {
        [SerializeField] private GameObject playerPrefab;

        [Header("Player Health Model")]
        [SerializeField] private int maxHealth;
        
        [Header("Player Health View")]
        [SerializeField] private TextMeshProUGUI viewHealthText;
        
        [Header("Player Movement Model")]
        [SerializeField] private float speed;
        [SerializeField] private float smoothTime;
        
        [Header("Player Movement View")]
        [SerializeField] private TextMeshProUGUI viewMoveInputText;

        private GameObject _playerInstance;
        private InputHandler _inputHandler;
        
        #region PlayerHealth

        private PlayerHealthController _healthController;
        private PlayerHealthModel _healthModel;
        private PlayerHealthPhysics _healthPhysics;
        private PlayerHealthView _healthView;

        #endregion
        
        #region PlayerMovement

        private PlayerMovementController _movementController;
        private PlayerMovementModel _movementModel;
        private PlayerMovementPhysics _movementPhysics;
        private PlayerMovementView _movementView;

        #endregion

        private void Awake()
        {
            _playerInstance = Instantiate(playerPrefab);
            _inputHandler = new InputHandler();

            InitHealthSystem();
            InitMovementSystem();
        }

        private void InitHealthSystem()
        {
            _healthPhysics = _playerInstance.GetComponent<PlayerHealthPhysics>();
            _healthView = _playerInstance.GetComponent<PlayerHealthView>();
            _healthView.Init(viewHealthText);
            _healthModel = new PlayerHealthModel(_healthPhysics, maxHealth);
                
            _healthController = new PlayerHealthController(_healthModel, _healthView);
        }

        private void InitMovementSystem()
        {
            _movementModel = new PlayerMovementModel(_inputHandler.BasicControls, speed, smoothTime);
            _movementPhysics = _playerInstance.GetComponent<PlayerMovementPhysics>();
            _movementPhysics.Init(_movementModel);
            _movementView = _playerInstance.GetComponent<PlayerMovementView>();
            _movementView.Init(viewMoveInputText);
                
            _movementController = new PlayerMovementController(_movementPhysics, _movementModel, _movementView, _healthModel);
        }
    }
}