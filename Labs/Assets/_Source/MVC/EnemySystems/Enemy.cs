using UnityEngine;

namespace MVC.EnemySystems
{
    public class Enemy : MonoBehaviour
    {
        [SerializeField] private int contactDamage;
        
        public int ContactDamage { get => contactDamage; }
    }
}