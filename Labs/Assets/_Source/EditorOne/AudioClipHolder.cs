using System;
using UnityEngine;

namespace EditorOne
{
    [System.Serializable]
    public class AudioClipHolder
    {
        [SerializeField] private AudioClip audioClip;
        [SerializeField] [Range(0f, 1f)] private float volume;
    }
}