using System.Collections;
using System.Collections.Generic;
using EditorOne;
using UnityEngine;
using AudioType = EditorOne.AudioType;

[CreateAssetMenu(fileName = "NewAudioClipHolder", menuName = "SOs/Audio/ClipHolder")]
public class AudioClipHoldersSO : ScriptableObject
{
    [SerializeField] private int id;

    [SerializeField] private AudioType audioType;
    [SerializeField] private List<AudioClipHolder> friendlyAudioList;
    [SerializeField] private List<AudioClipHolder> neutralAudioList;
    [SerializeField] private List<AudioClipHolder> dangerousAudioList;
    
    [SerializeField] private string text;
}
