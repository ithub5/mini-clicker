using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace EditorOne
{
    [CustomEditor(typeof(AudioClipHoldersSO))]
    public class AudioClipHoldersSOEditor : Editor
    {
        private SerializedProperty _id;

        private SerializedProperty _audioType;
        private Dictionary<AudioType, SerializedProperty> _audioLists;
        
        private SerializedProperty _text;

        private bool _showList;
        private bool _showText;

        private GUILayoutOption[] _textAreaLayoutOptions;

        private void Awake()
        {
            _id = serializedObject.FindProperty("id");

            _audioType = serializedObject.FindProperty("audioType");

            _audioLists = new Dictionary<AudioType, SerializedProperty>();
            for (int i = 0; i < Enum.GetValues(typeof(AudioType)).Length; i++)
            {
                AudioType currentAudioType = (AudioType)i;
                SerializedProperty currentList =
                    serializedObject.FindProperty($"{currentAudioType.ToString()}AudioList");
                
                _audioLists.Add(currentAudioType, currentList);
            }
            
            _text = serializedObject.FindProperty("text");

            _textAreaLayoutOptions = new [] { GUILayout.MinHeight(10f), GUILayout.ExpandHeight(true), GUILayout.MaxHeight(80f)};
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.BeginHorizontal();
            
            DrawButtons();
            
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.PropertyField(_id);

            EditorGUILayout.PropertyField(_audioType);

            if (_showList)
            {
                EditorGUILayout.PropertyField(_audioLists[(AudioType)_audioType.enumValueIndex]);
            }

            if (_showText)
            {
                _text.stringValue = EditorGUILayout.TextArea(_text.stringValue, _textAreaLayoutOptions);
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawButtons()
        {
            if (GUILayout.Button("ShowList"))
            {
                _showList = !_showList;
                _showText = false;
            }
            
            if (GUILayout.Button("Showtext"))
            {
                _showText = !_showText;
                _showList = false;
            }
        }
    }
}