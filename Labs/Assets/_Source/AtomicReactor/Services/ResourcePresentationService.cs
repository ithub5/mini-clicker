using System;
using System.Linq;
using AtomicReactor.InGameResources.Presentation;
using AtomicReactor.Utils.Enums;
using UnityEngine;
using UnityEngine.UI;

namespace AtomicReactor.Services
{
    public class ResourcePresentationService
    {
        public static ResourcePresentationService Instance { get; private set; } = new ResourcePresentationService();

        private static ResourcesPresentationDataSO _resourcesPresentationDataSO;
        private const string PathToResources = "SOs/AtomicReactor/Resources/ResourcesPresentationDataSO";
        
        static ResourcePresentationService()
        {
            _resourcesPresentationDataSO = UnityEngine.Resources.Load<ResourcesPresentationDataSO>(PathToResources);
        }

        private ResourcePresentationService() { }

        /// <summary>
        /// activeInactive - true - active, false inactive
        /// </summary>
        /// <param name="image"></param>
        /// <param name="resourceType"></param>
        /// <param name="activeInActive"></param>
        public bool SetResourceSprite(Image image, ResourceType resourceType, bool activeInActive)
        {
            ResourcePresentationData resourcePresentationData = _resourcesPresentationDataSO.Resources.FirstOrDefault(
                    resource => resource.ResourceType == resourceType);

            if (resourcePresentationData == null)
            {
                return false;
            }

            image.sprite = activeInActive
                ? resourcePresentationData.ActiveSprite
                : resourcePresentationData.InactiveSprite;

            return true;
        }
    }
}