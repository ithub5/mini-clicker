using System;
using System.Collections;
using UnityEngine;

namespace AtomicReactor.Services
{
    public class CoroutineService : MonoBehaviour
    {
        public static CoroutineService Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                return;
            }
            
            Destroy(this);
        }

        private CoroutineService() {}

        public Coroutine DoCoroutine(float waitTime, Action onEndAction)
        {
            return StartCoroutine(OnEndActionCoroutine(waitTime, onEndAction));
        }

        public void UndoCoroutine(Coroutine coroutine)
        {
            StopCoroutine(coroutine);
        }

        private IEnumerator OnEndActionCoroutine(float waitTime, Action onEndAction)
        {
            yield return new WaitForSeconds(waitTime);
            
            onEndAction?.Invoke();
        }
    }
}