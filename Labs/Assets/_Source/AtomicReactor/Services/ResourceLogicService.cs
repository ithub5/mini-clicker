using System;
using System.Linq;
using AtomicReactor.InGameResources.Logic;
using AtomicReactor.Utils.Enums;
using UnityEngine;

namespace AtomicReactor.Services
{
    public class ResourceLogicService
    {
        public static ResourceLogicService Instance { get; private set; } = new ResourceLogicService();

        private static ResourcesLogicDataSO _resourcesLogicDataSO;
        private const string PathToResources = "SOs/AtomicReactor/Resources/ResourcesLogicDataSO";
        
        static ResourceLogicService()
        {
            _resourcesLogicDataSO = UnityEngine.Resources.Load<ResourcesLogicDataSO>(PathToResources);
        }

        private ResourceLogicService() { }

        public bool GetResourceEnrichmentTime(ResourceType resourceType, out float resourceEnrichmentTime)
        {
            ResourceLogicData resourceLogicData = _resourcesLogicDataSO.Resources.FirstOrDefault(
                resource => resource.ResourceType == resourceType);

            if (resourceLogicData == null)
            {
                resourceEnrichmentTime = -1;
                return false;
            }

            resourceEnrichmentTime = resourceLogicData.EnrichmentTime;

            return true;
        }
        
        public bool GetResourceDecayTime(ResourceType resourceType, out float resourceDecayTime)
        {
            ResourceLogicData resourceLogicData = _resourcesLogicDataSO.Resources.FirstOrDefault(
                resource => resource.ResourceType == resourceType);

            if (resourceLogicData == null)
            {
                resourceDecayTime = -1;
                return false;
            }

            resourceDecayTime = resourceLogicData.DecayTime;

            return true;
        }
    }
}