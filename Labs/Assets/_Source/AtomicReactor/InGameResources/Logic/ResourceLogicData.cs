using System;
using AtomicReactor.Utils.Enums;
using UnityEngine;

namespace AtomicReactor.InGameResources.Logic
{
    [Serializable]
    public class ResourceLogicData
    {
        [field: SerializeField] public ResourceType ResourceType { get; private set; }
        [field: SerializeField] public float EnrichmentTime { get; private set; }
        [field: SerializeField] public float DecayTime { get; private set; }
    }

}

