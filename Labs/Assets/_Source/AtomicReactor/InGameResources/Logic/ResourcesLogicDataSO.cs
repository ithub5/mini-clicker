using UnityEngine;

namespace AtomicReactor.InGameResources.Logic
{
    [CreateAssetMenu(menuName = "SOs/Resources/LogicDataSO", fileName = "ResourcesLogicDataSO")]
    public class ResourcesLogicDataSO : ScriptableObject
    {
        [field: SerializeField] public ResourceLogicData[] Resources { get; private set; }
    }
}