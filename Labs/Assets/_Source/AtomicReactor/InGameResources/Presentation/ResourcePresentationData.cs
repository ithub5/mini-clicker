using System;
using AtomicReactor.Utils.Enums;
using UnityEngine;

namespace AtomicReactor.InGameResources.Presentation
{
    [Serializable]
    public class ResourcePresentationData
    {
        [field: SerializeField] public ResourceType ResourceType { get; private set; }
        [field: SerializeField] public Sprite ActiveSprite { get; private set; }
        [field: SerializeField] public Sprite InactiveSprite { get; private set; }
    }
}