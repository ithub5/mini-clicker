using UnityEngine;

namespace AtomicReactor.InGameResources.Presentation
{
    [CreateAssetMenu(menuName = "SOs/Resources/PresentationDataSO", fileName = "ResourcesPresentationDataSO")]
    public class ResourcesPresentationDataSO : ScriptableObject
    {
        [field: SerializeField] public ResourcePresentationData[] Resources { get; private set; }
    }
}