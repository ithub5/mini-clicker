using System;
using AtomicReactor.Services;
using AtomicReactor.Utils.Enums;
using UnityEngine;
using UnityEngine.UI;

namespace AtomicReactor.InGameResources
{
    public class ResourceAnchor : MonoBehaviour
    {
        [SerializeField] private ResourceType resourceType;

        [Header("Components")]
        [SerializeField] private Button btn;
        [SerializeField] private Image image;

        private float _enrichmentTime;
        private float _decayTime;

        private bool _decayOrEnrichment;
        private Coroutine _decayCoroutine;
        
        public event Action OnDecayComplete;

        public bool Init()
        {
            bool enrichmentTimeGetSuccess =
                ResourceLogicService.Instance.GetResourceEnrichmentTime(resourceType, out _enrichmentTime);
            bool decayTimeGetSuccess =
                ResourceLogicService.Instance.GetResourceEnrichmentTime(resourceType, out _decayTime);

            if (!enrichmentTimeGetSuccess || !decayTimeGetSuccess)
            {
                return false;
            }
            
            btn.onClick.AddListener(StartEnrichment);
            return true;
        }

        public void StartCycle()
        {
            StartDecay();
        }

        private void StartDecay()
        {
            _decayOrEnrichment = true;
            
            UpdateResource();
            
            _decayCoroutine = CoroutineService.Instance.DoCoroutine(_decayTime, () => OnDecayComplete?.Invoke());
        }

        private void StartEnrichment()
        {
            _decayOrEnrichment = false;
            
            UpdateResource();
            
            CoroutineService.Instance.UndoCoroutine(_decayCoroutine);
            CoroutineService.Instance.DoCoroutine(_enrichmentTime, StartDecay);
        }

        private void UpdateResource()
        {
            btn.interactable = _decayOrEnrichment;
            ResourcePresentationService.Instance.SetResourceSprite(image, resourceType, _decayOrEnrichment);
        }
    }
}
