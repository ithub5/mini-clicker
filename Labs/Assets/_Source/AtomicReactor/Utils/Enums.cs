namespace AtomicReactor.Utils.Enums
{
    public enum ResourceType
    {
        Uranium = 0, 
        Plutonium = 1
    }
}