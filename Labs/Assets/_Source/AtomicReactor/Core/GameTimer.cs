using System;
using TMPro;
using UnityEngine;

namespace AtomicReactor.Core
{
    public class GameTimer : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI timerText;

        private float _timePassed;
        private bool _countTime;

        public void Start()
        {
            _timePassed = 0f;
            _countTime = true;
        }

        public void Stop() =>
            _countTime = false;

        private void Update()
        {
            if (!_countTime)
            {
                return;
            }

            _timePassed += Time.deltaTime;
            timerText.text = _timePassed.ToString("0.00");
        }
    }
}