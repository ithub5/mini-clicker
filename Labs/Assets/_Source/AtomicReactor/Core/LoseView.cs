using UnityEngine;

namespace AtomicReactor.Core
{
    public class LoseView : MonoBehaviour
    {
        [SerializeField] private GameObject losePanel;

        public void Show()
        {
            losePanel.SetActive(true);
        }
    }
}