using AtomicReactor.InGameResources;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace AtomicReactor.Core
{
    public class Game
    {
        private const string SETUP_ERROR_ONE = "Game SetUp Error №1 'some of resources in play dont have logic data'";

        private ResourceAnchor[] _resourceAnchors;
        
        private GameTimer _gameTimer;
        
        private LoseView _loseView;
        private Button _restartBtn;

        private bool _lost;
        
        public Game(ResourceAnchor[] resourceAnchors, GameTimer gameTimer, LoseView loseView, Button restartBtn)
        {
            _resourceAnchors = resourceAnchors;

            _gameTimer = gameTimer;
            
            _loseView = loseView;
            _restartBtn = restartBtn;
        }

        public bool Init()
        {
            for (int indx = 0; indx < _resourceAnchors.Length; indx++)
            {
                if (!_resourceAnchors[indx].Init())
                {
                    Debug.LogError(SETUP_ERROR_ONE);
                    return false;
                }

                _resourceAnchors[indx].OnDecayComplete += Lose;
            }

            _restartBtn.onClick.AddListener(Restart);
            
            return true;
        }

        public void Start()
        {
            for (int indx = 0; indx < _resourceAnchors.Length; indx++)
            {
                _resourceAnchors[indx].StartCycle();
            }
            
            _gameTimer.Start();
            _lost = false;
        }

        private void Lose()
        {
            if (_lost)
            {
                return;
            } 
            
            _lost = true;
            
            _gameTimer.Stop();
            _loseView.Show();
        }

        private void Restart()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
