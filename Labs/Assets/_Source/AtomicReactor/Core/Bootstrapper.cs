    using AtomicReactor.InGameResources;
using UnityEngine;
using UnityEngine.UI;

namespace AtomicReactor.Core
{
    public class Bootstrapper : MonoBehaviour
    {
        [SerializeField] private ResourceAnchor[] resourceAnchors;

        [SerializeField] private GameTimer gameTimer;
        
        [SerializeField] private LoseView loseView;
        [SerializeField] private Button restartBtn;
        
        private Game _game;

        private void Start()
        {
            _game = new Game(resourceAnchors, gameTimer, loseView, restartBtn);

            if (!_game.Init())
            {
                return;
            }
            
            _game.Start();
        }
    }
}
