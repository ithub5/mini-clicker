using StaticCalculator.Data;
using UnityEditor.Experimental;
using UnityEngine;

namespace StaticCalculator
{
    public static class FormulasCalculator
    {
        private static ConstantsSO _constantsSO;
        
        static FormulasCalculator()
        {
            _constantsSO = Resources.Load("SOs/StaticCalculator/Constants") as ConstantsSO;
        }

        public static float Speed(float path, float time)
        {
            return path / time;
        }

        public static float SecondNewton(float force, float mass)
        {
            return force * mass;
        }

        public static float StartHeight(float time)
        {
            return _constantsSO.g * (time * time) / 2;
        }

        public static float MechanicWork(float force, float time)
        {
            return force * time;
        }

        public static float SpringPendulumPeriod(float mass, float springToughness)
        {
            return 2 * _constantsSO.Pi * Mathf.Sqrt(mass / springToughness);
        }
    }
}