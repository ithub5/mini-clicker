using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StaticCalculator.Data
{
    [CreateAssetMenu(fileName = "Constants", menuName = "SOs/StaticCalculator/Constants")]
    public class ConstantsSO : ScriptableObject
    {
        [field: SerializeField] public float g { get; private set; }
        [field: SerializeField] public float Pi { get; private set; }
    }
}