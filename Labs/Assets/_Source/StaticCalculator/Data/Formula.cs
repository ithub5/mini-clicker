using StaticCalculator.Utils;
using UnityEngine;

namespace StaticCalculator.Data
{
    [System.Serializable]
    public class Formula
    {
        [field: SerializeField] public FormulaType Type { get; private set; }
        [field: SerializeField] public string Equation { get; private set; }
    }
}