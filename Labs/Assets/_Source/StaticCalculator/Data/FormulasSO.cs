using UnityEngine;

namespace StaticCalculator.Data
{
    [CreateAssetMenu(fileName = "NewFormulas", menuName = "SOs/StaticCalculator/Formulas")]
    public class FormulasSO : ScriptableObject
    {
        [field: SerializeField] public Formula[] Formulas { get; private set; }
    }
}