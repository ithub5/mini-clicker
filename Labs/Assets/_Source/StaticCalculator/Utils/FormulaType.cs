namespace StaticCalculator.Utils
{
    public enum FormulaType
    {
        speed = 0,
        secondNewton = 1,
        startHeight = 2,
        mechanicWork = 3,
        springPendulumPeriod = 4
    }
}