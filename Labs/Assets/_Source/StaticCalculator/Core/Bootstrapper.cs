using System;
using StaticCalculator.Data;
using StaticCalculator.UI;
using StaticCalculator.Utils;
using UnityEngine;

namespace StaticCalculator.Core
{
    public class Bootstrapper : MonoBehaviour
    {
        [SerializeField] private FormulasSO formulasSO;

        [SerializeField] private FormulaUIView[] formulasView;

        private FormulaUILogic[] _formulasLogic;

        private void Awake()
        {
            _formulasLogic = new FormulaUILogic[formulasSO.Formulas.Length];
            
            for (int i = 0; i < formulasSO.Formulas.Length; i++)
            {
                FormulaType formulaType = formulasSO.Formulas[i].Type;
                _formulasLogic[i] = new FormulaUILogic(formulasView[i], formulaType.ToString(), formulaType);
            }
        }
    }
}