using System;
using StaticCalculator.Utils;

namespace StaticCalculator.UI
{
    public class FormulaUILogic
    {
        private FormulaUIView _view;

        private FormulaType _formulaType;

        public FormulaUILogic(FormulaUIView view, string formula, FormulaType formulaType)
        {
            _view = view;
            
            _view.UpdateFormulaText(formula);
            _view.calculateButton.onClick.AddListener(Calculate);
            
            _formulaType = formulaType;
        }

        private void Calculate()
        {
            float result = 0f;
            float[] values = _view.Values;
            
            switch (_formulaType)
            {
                case FormulaType.speed:
                    result = FormulasCalculator.Speed(values[0], values[1]);
                    break;
                case FormulaType.secondNewton:
                    result = FormulasCalculator.SecondNewton(values[0], values[1]);
                    break;
                case FormulaType.startHeight:
                    result = FormulasCalculator.StartHeight(values[0]);
                    break;
                case FormulaType.mechanicWork:
                    result = FormulasCalculator.MechanicWork(values[0], values[1]);
                    break;
                case FormulaType.springPendulumPeriod:
                    result = FormulasCalculator.SpringPendulumPeriod(values[0], values[1]);
                    break;
            } 
            
            _view.UpdateResultText(result.ToString());
        }
    }
}