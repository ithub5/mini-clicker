using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FormulaUIView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI formulaText;
    [SerializeField] private TMP_InputField[] values;
    [SerializeField] private TextMeshProUGUI resultText;

    [field: SerializeField] public Button calculateButton;

    public float[] Values => values.Select(value =>
    {
        if (value != null) { return float.Parse(value.text); } 
        return 0f;
    }).ToArray();

    private void Start()
    {
        for (int i = 0; i < values.Length; i++)
        {
            if (values[i] == null)
            {
                continue;
            }

            values[i].characterValidation = TMP_InputField.CharacterValidation.Digit;
            values[i].onSubmit.AddListener(ValuesSubmitted);
        }
        
        ValuesSubmitted(default);
    }

    public void UpdateFormulaText(string newFormula)
    {
        formulaText.text = newFormula;
    }

    public void UpdateResultText(string newResult)
    {
        resultText.text = newResult;
    }

    private void ValuesSubmitted(string value)
    {
        for (int i = 0; i < values.Length; i++)
        {
            if (values[i] != null && string.IsNullOrEmpty(values[i].text))
            {
                calculateButton.interactable = false;
                return;
            }
        }

        calculateButton.interactable = true;
    }
}
