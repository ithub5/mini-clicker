using System;
using Mono.Cecil.Cil;
using StateMachinePattern.Utils;
using UnityEngine;

namespace StateMachinePattern.StateSystem
{
    public abstract class AState
    {
        protected AStatesController owner;
        
        public Type[] NextStates { get; private set; }

        private Action<AState> onEnter;
        private Action<AState> onUpdate;
        private Action<AState> onExit;

        protected AState(AStatesController owner, params Type[] nextStates)
        {
            this.owner = owner;

            NextStates = nextStates;
        }

        public virtual void Enter()
        {
            onEnter?.Invoke(this);
        }

        public virtual void Update()
        {
            onUpdate?.Invoke(this);
        }

        public virtual void Exit()
        {
            onExit?.Invoke(this);
        }

        public void ModifyActions(StateActionType actionType, bool addRemove, params Action<AState>[] actionsIn)
        {
            switch (actionType)
            {
                case StateActionType.enter:
                    OperateActions(ref onEnter, addRemove, actionsIn);
                    break;
                case StateActionType.update:
                    OperateActions(ref onUpdate, addRemove, actionsIn);
                    break;
                case StateActionType.exit:
                    OperateActions(ref onExit, addRemove, actionsIn);
                    break;
            }
        }

        private void OperateActions(ref Action<AState> actionToAddTo, bool addRemove, Action<AState>[] actionsIn)
        {
            if (addRemove)
            {
                for (int i = 0; i < actionsIn.Length; i++)
                {
                    actionToAddTo += actionsIn[i];
                }
                
                return;
            }
            
            for (int i = 0; i < actionsIn.Length; i++)
            {
                actionToAddTo -= actionsIn[i];
            }
        }
    }
}