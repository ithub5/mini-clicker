using System;
using System.Linq;

namespace StateMachinePattern.StateSystem
{
    public class StateMachine
    {
        private AStatesController _statesController;
        
        public AState CurrentState { get; private set; }

        public StateMachine(AStatesController statesController, Type initState)
        {
            _statesController = statesController;
            
            ChangeState(initState);
        }

        public void ToNextState()
        {
            if (CurrentState == null || CurrentState.NextStates.Length <= 0)
            {
                return;
            }
            
            ToState(CurrentState.NextStates[0]);
        }

        public bool ToState(Type nextState)
        {
            if (CurrentState != null && !CurrentState.NextStates.Contains(nextState))
            {
                return false;
            }
            
            ChangeState(nextState);
            return true;
        }

        
        private void ChangeState(Type type)
        {
            CurrentState?.Exit();
            CurrentState = _statesController.States[type];
            CurrentState.Enter();
        }

        public void Update()
        {
            CurrentState.Update();
        }
    }
}