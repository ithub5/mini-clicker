using System;
using System.Collections.Generic;

namespace StateMachinePattern.StateSystem
{
    public abstract class AStatesController
    {
        public Dictionary<Type, AState> States { get; protected set; }

        public AStatesController()
        {
            States = new Dictionary<Type, AState>();
        }
    }
}