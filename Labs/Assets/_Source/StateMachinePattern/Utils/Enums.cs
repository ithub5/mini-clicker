namespace StateMachinePattern.Utils
{
    public enum StateActionType
    {
        enter = 0,
        update = 1,
        exit = 2
    }
}