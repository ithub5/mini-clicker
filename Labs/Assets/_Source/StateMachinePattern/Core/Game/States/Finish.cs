using System;
using StateMachinePattern.StateSystem;

namespace StateMachinePattern.Core.Game.States
{
    public class Finish : AState
    {
        public Finish(AStatesController owner, params Type[] nextStates) : base(owner, nextStates)
        {
        }
    }
}