using StateMachinePattern.InputSystem;
using StateMachinePattern.StateSystem;
using StateMachinePattern.Utils;
using UnityEngine;

namespace StateMachinePattern.Core.Game.States
{
    public class GameStatesController : AStatesController
    {
        private InputHandler _input;
        private SpriteRenderer _playerRenderer;
        
        public GameStatesController(InputHandler input, SpriteRenderer playerRenderer)
        {
            _input = input;
            _playerRenderer = playerRenderer;
            
            States.Add(typeof(Play), new Play(this, typeof(Pause), typeof(Finish)));
            States.Add(typeof(Pause), new Pause(this, typeof(Play), typeof(Finish)));
            States.Add(typeof(Finish), new Finish(this));

            InitStates();
        }

        private void EnterPlayState(AState state) =>
            _input.PlayerActions.Enable();
        
        private void ExitPlayState(AState state) =>
            _input.PlayerActions.Disable();

        private void EnterFinishState(AState state) =>
            _playerRenderer.color = Color.green;

        private void InitStates()
        {
            States[typeof(Play)].ModifyActions(StateActionType.enter, true, EnterPlayState);
            States[typeof(Play)].ModifyActions(StateActionType.exit, true, ExitPlayState);
            
            States[typeof(Finish)].ModifyActions(StateActionType.enter, true, EnterFinishState);
        }
    }
}