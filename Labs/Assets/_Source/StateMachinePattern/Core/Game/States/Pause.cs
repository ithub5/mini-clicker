using System;
using StateMachinePattern.StateSystem;

namespace StateMachinePattern.Core.Game.States
{
    public class Pause : AState
    {
        public Pause(AStatesController owner, params Type[] nextStates) : base(owner, nextStates)
        {
        }
    }
}