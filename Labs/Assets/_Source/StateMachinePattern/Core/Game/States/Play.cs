using System;
using StateMachinePattern.StateSystem;

namespace StateMachinePattern.Core.Game.States
{
    public class Play : AState
    {
        public Play(AStatesController owner, params Type[] nextStates) : base(owner, nextStates)
        {
        }
    }
}