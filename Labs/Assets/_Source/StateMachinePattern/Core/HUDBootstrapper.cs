using StateMachinePattern.StateSystem;
using StateMachinePattern.UI.HUD;
using StateMachinePattern.Utils;
using TMPro;
using UnityEditor;

namespace StateMachinePattern.Core
{
    public class HUDBootstrapper
    {
        private AStatesController _controllerToPresent;

        private HUDView _view;

        public HUDBootstrapper(AStatesController statesController, TextMeshProUGUI stateText)
        {
            _controllerToPresent = statesController;

            _view = new HUDView(stateText);

            foreach (var state in _controllerToPresent.States)
            {
                state.Value.ModifyActions(StateActionType.enter, true, _view.UpdateStateText);
            }
        }
    }
}