using System;
using StateMachinePattern.Core.Game.States;
using StateMachinePattern.InputSystem;
using StateMachinePattern.PlayerSystem;
using StateMachinePattern.PlayerSystem.Data;
using StateMachinePattern.PlayerSystem.States.AttackStates;
using StateMachinePattern.PlayerSystem.States.MoveStates;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using StateMachine = StateMachinePattern.StateSystem.StateMachine;

namespace StateMachinePattern.Core
{
    public class Bootstrapper : MonoBehaviour
    {
        [Header("Player Physics")] 
        [SerializeField] private Rigidbody2D playerRb;
        [SerializeField] private PlayerPhysicsSettingsSO playerPhysicsSettingsSO;
        [SerializeField] private Transform firePoint;
        
        [Header("Player View")]
        [SerializeField] private SpriteRenderer playerRenderer;
        [SerializeField] private GameObject gameObjectAoE;
        [SerializeField] private PlayerViewSettingsSO playerViewSettingsSO;

        [Header("HUD View")] 
        [SerializeField] private TextMeshProUGUI currentStateText;
        
        private InputHandler _input;

        private PlayerSystemBootstrapper _playerBootstrapper;
        private HUDBootstrapper _hudBootstrapper;

        private StateMachine _gameStateMachine;
        private StateMachine _attackStateMachine;
        private StateMachine _moveStateMachine;

        private GameStatesController _gameStatesController;
        private PlayerAttackStatesController _attackStatesController;
        private PlayerMoveStatesController _moveStatesController;

        private void Awake()
        {
            _input = new InputHandler();

            _playerBootstrapper = new PlayerSystemBootstrapper(_input, playerRb, playerPhysicsSettingsSO, firePoint,
                playerRenderer, gameObjectAoE, playerViewSettingsSO, out _attackStatesController, out _moveStatesController);
            _hudBootstrapper = new HUDBootstrapper(_attackStatesController, currentStateText);

            _gameStatesController = new GameStatesController(_input, playerRenderer);
            
            _attackStateMachine = new StateMachine(_attackStatesController, typeof(Shoot));
            _moveStateMachine = new StateMachine(_moveStatesController, typeof(Move));

            _gameStateMachine = new StateMachine(_gameStatesController, typeof(Play));
            
            Bind();
            _input.Enable();
        }
        
        private void Bind()
        {
            _input.StateMachineActions.ChangeAttackState.performed += NextAttackState;

            _input.StateMachineActions.Pause.performed += PausePlayGame;
            _input.StateMachineActions.Finish.performed += FinishGame;
        }

        private void Update()
        {
            _attackStateMachine.Update();
            _moveStateMachine.Update();
        }
        
        private void NextAttackState(InputAction.CallbackContext ctx) =>
            _attackStateMachine.ToNextState();

        private void PausePlayGame(InputAction.CallbackContext ctx)
        {
            Type nextGameState =
                _gameStateMachine.CurrentState.GetType() == typeof(Play) ? typeof(Pause) : typeof(Play);
            
            _gameStateMachine.ToState(nextGameState);
        }

        private void FinishGame(InputAction.CallbackContext ctx) =>
            _gameStateMachine.ToState(typeof(Finish));
    }
}