using StateMachinePattern.InputSystem;
using StateMachinePattern.PlayerSystem;
using StateMachinePattern.PlayerSystem.Data;
using StateMachinePattern.PlayerSystem.States.AttackStates;
using StateMachinePattern.PlayerSystem.States.MoveStates;
using UnityEngine;

namespace StateMachinePattern.Core
{
    public class PlayerSystemBootstrapper
    {
        private InputHandler _input;
        
        private PlayerInvoker _invoker;
        private PlayerPhysics _physics;
        private PlayerView _view;

        public PlayerSystemBootstrapper(InputHandler input, Rigidbody2D rb, PlayerPhysicsSettingsSO physicsSettingsSO, 
            Transform firePoint, SpriteRenderer renderer, GameObject gameObjectAoE, PlayerViewSettingsSO viewSettingsSO,
            out PlayerAttackStatesController attackStatesController, out PlayerMoveStatesController moveStatesController)
        {
            _input = input;
            
            _physics = new PlayerPhysics(rb, physicsSettingsSO, firePoint);
            _view = new PlayerView(renderer, gameObjectAoE, viewSettingsSO);
            
            attackStatesController = new PlayerAttackStatesController(_physics, _view);
            moveStatesController = new PlayerMoveStatesController(_physics);

            _invoker = new PlayerInvoker(_input, _physics, attackStatesController);
            _invoker.Bind();
        }
    }
}