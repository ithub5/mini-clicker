using StateMachinePatternSecondVariant.InputSystem;

namespace StateMachinePattern.InputSystem
{
    public class InputHandler
    {
        private MainAction _actions;

        public MainAction.PlayerActions PlayerActions => _actions.Player;
        public MainAction.StateMachineActions StateMachineActions => _actions.StateMachine;

        public InputHandler()
        {
            _actions = new MainAction();
        }

        public void Enable()
        {
            _actions.Enable();
        }

        public void Disable()
        {
            _actions.Disable();
        }
    }
}