using StateMachinePattern.StateSystem;
using TMPro;

namespace StateMachinePattern.UI.HUD
{
    public class HUDView
    {
        private TextMeshProUGUI _stateText;

        public HUDView(TextMeshProUGUI stateText)
        {
            _stateText = stateText;
        }
        
        public void UpdateStateText(AState state)
        {
            string stateName = state.ToString().Split(".")[^1];
            _stateText.text = stateName;
        }
    }
}