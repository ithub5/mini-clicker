using StateMachinePattern.PlayerSystem.Data;
using UnityEngine;

namespace StateMachinePattern.PlayerSystem
{
    public class PlayerView
    {
        private SpriteRenderer _renderer;
        private GameObject _gameObjectAoE;
        private PlayerViewSettingsSO _settingsSO;

        private bool _isVisible;
        
        public PlayerView(SpriteRenderer renderer, GameObject gameObjectAoE, PlayerViewSettingsSO settingsSO)
        {
            _renderer = renderer;
            _gameObjectAoE = gameObjectAoE;

            _settingsSO = settingsSO;

            _isVisible = true;
        }

        public void VisibilityOnOff()
        {
            _isVisible = !_isVisible;
            _renderer.color = _isVisible ? Color.white : new Color(1, 1, 1, _settingsSO.Visibility);
        }

        public void VisibilityOnOff(bool onOff)
        {
            _isVisible = onOff;
            _renderer.color = _isVisible ? Color.white : new Color(1, 1, 1, _settingsSO.Visibility);
        }
        
        public void AoEOnOff()
        {
            _gameObjectAoE.SetActive(!_gameObjectAoE.activeSelf);
        }
        
        public void AoEOnOff(bool onOff)
        {
            _gameObjectAoE.SetActive(onOff);
        }
    }
}