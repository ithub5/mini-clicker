using System;
using StateMachinePattern.InputSystem;
using StateMachinePattern.PlayerSystem.States.AttackStates;
using StateMachinePattern.PlayerSystem.States.MoveStates;
using StateMachinePattern.StateSystem;
using StateMachinePattern.Utils;
using UnityEngine;
using UnityEngine.InputSystem;

namespace StateMachinePattern.PlayerSystem
{
    public class PlayerInvoker
    {
        private readonly InputHandler _input;

        private PlayerPhysics _physics;
        
        private readonly PlayerAttackStatesController _playerAttackStates;

        public PlayerInvoker(InputHandler input, PlayerPhysics physics, PlayerAttackStatesController playerAttackStates)
        {
            _input = input;

            _physics = physics;
            
            _playerAttackStates = playerAttackStates;
        }

        public void Bind()
        {
            _input.PlayerActions.Movement.performed += UpdateInput;
            _input.PlayerActions.Movement.canceled += UpdateInput;
            
            _input.PlayerActions.Attack.performed += PerformAttack;
        }
        
        private void UpdateInput(InputAction.CallbackContext ctx)
        {
            Vector2 newInput = ctx.ReadValue<Vector2>();
            
            _physics.UpdateInput(newInput);
        }

        private void PerformAttack(InputAction.CallbackContext ctx)
        {
            _playerAttackStates.AttackAction?.Invoke();
        }
    }
}