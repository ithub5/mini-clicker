using StateMachinePattern.PlayerSystem.Data;
using UnityEngine;

namespace StateMachinePattern.PlayerSystem
{
    public class PlayerPhysics
    {
        private Rigidbody2D _rb;
        
        private PlayerPhysicsSettingsSO _settingsSO;

        private Transform _firePoint;

        private Vector2 _input;
        private Vector2 _acceleration;

        public PlayerPhysics(Rigidbody2D rb, PlayerPhysicsSettingsSO settingsSo, Transform firePoint)
        {
            _rb = rb;
            
            _settingsSO = settingsSo;

            _firePoint = firePoint;
        }

        public void UpdateInput(Vector2 newInput)
        {
            _input = newInput;
        }

        public void Move()
        {
            if (_input.magnitude <= 0f && _acceleration.magnitude <= 0f)
            {
                return;
            }

            Vector2 targetVelocity = _input * _settingsSO.MoveSpeed;
            _rb.velocity = Vector2.SmoothDamp(_rb.velocity, targetVelocity, ref _acceleration,
                _settingsSO.MoveSmoothTime);
        }

        public void Shoot()
        {
            Object.Instantiate(_settingsSO.BulletPrefab, _firePoint);
        }
    }
}