using System;
using StateMachinePattern.StateSystem;

namespace StateMachinePattern.PlayerSystem.States.MoveStates
{
    public class Move : AState
    {
        public Move(AStatesController owner, params Type[] nextStates) : base(owner, nextStates)
        {
        }
    }
}