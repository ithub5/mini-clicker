using StateMachinePattern.InputSystem;
using StateMachinePattern.StateSystem;
using StateMachinePattern.Utils;
using UnityEngine;
using UnityEngine.InputSystem;

namespace StateMachinePattern.PlayerSystem.States.MoveStates
{
    public class PlayerMoveStatesController : AStatesController
    {
        private PlayerPhysics _physics;

        public PlayerMoveStatesController(PlayerPhysics physics)
        {
            _physics = physics;
            
            States.Add(typeof(Move), new Move(this));
            InitStates();
        }

        private void UpdateMoveState(AState state)
        {
            _physics.Move();
        }

        private void InitStates()
        {
            States[typeof(Move)].ModifyActions(StateActionType.update, true, UpdateMoveState);
        }
    }
}