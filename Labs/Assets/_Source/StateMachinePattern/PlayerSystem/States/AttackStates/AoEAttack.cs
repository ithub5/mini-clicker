using System;
using StateMachinePattern.StateSystem;

namespace StateMachinePattern.PlayerSystem.States.AttackStates
{
    public class AoEAttack : AState
    {
        public AoEAttack(AStatesController owner, params Type[] nextStates) : base(owner, nextStates)
        {
        }
    }
}