using System;
using StateMachinePattern.StateSystem;
using StateMachinePattern.Utils;
using UnityEngine;

namespace StateMachinePattern.PlayerSystem.States.AttackStates
{
    public class PlayerAttackStatesController : AStatesController
    {
        private PlayerPhysics _physics;
        private PlayerView _view;

        public Action AttackAction { get; private set; }

        public PlayerAttackStatesController(PlayerPhysics physics, PlayerView view)
        {
            _physics = physics;
            _view = view;
            
            States.Add(typeof(Shoot), new Shoot(this, typeof(AoEAttack)));
            States.Add(typeof(AoEAttack), new AoEAttack(this, typeof(Visibility)));
            States.Add(typeof(Visibility), new Visibility(this, typeof(Shoot)));
            InitStates();
        }

        private void EnterShootState(AState state) =>
            AttackAction = _physics.Shoot;

        private void EnterAoEState(AState state) =>
            AttackAction = _view.AoEOnOff;

        private void ExitAoEState(AState state) =>
            _view.AoEOnOff(false);

        private void EnterVisibilityState(AState state) =>
            AttackAction = _view.VisibilityOnOff;

        private void ExitVisibilityState(AState state) =>
            _view.VisibilityOnOff(true);

        private void InitStates()
        {
            States[typeof(Shoot)].ModifyActions(StateActionType.enter, true, EnterShootState);
            
            States[typeof(AoEAttack)].ModifyActions(StateActionType.enter, true, EnterAoEState);
            States[typeof(AoEAttack)].ModifyActions(StateActionType.exit, true, ExitAoEState);
            
            States[typeof(Visibility)].ModifyActions(StateActionType.enter, true, EnterVisibilityState);
            States[typeof(Visibility)].ModifyActions(StateActionType.exit, true, ExitVisibilityState);
        }
    }
}