using System;
using StateMachinePattern.StateSystem;

namespace StateMachinePattern.PlayerSystem.States.AttackStates
{
    public class Visibility : AState
    {
        public Visibility(AStatesController owner, params Type[] nextStates) : base(owner, nextStates)
        {
        }
    }
}