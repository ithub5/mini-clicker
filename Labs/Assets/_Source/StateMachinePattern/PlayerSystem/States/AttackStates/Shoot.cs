using System;
using StateMachinePattern.StateSystem;

namespace StateMachinePattern.PlayerSystem.States.AttackStates
{
    public class Shoot : AState
    {
        public Shoot(AStatesController owner, params Type[] nextStates) : base(owner, nextStates)
        {
        }
    }
}