using UnityEngine;

namespace StateMachinePattern.PlayerSystem.Data
{
    [CreateAssetMenu(fileName = "NewPlayerPhysicsSettings", menuName = "SOs/StateMachine/Player/PhysicsSettings")]
    public class PlayerPhysicsSettingsSO : ScriptableObject
    {
        [field: Header("Movement")]
        [field: SerializeField] public float MoveSpeed { get; private set; }
        [field: SerializeField] public float MoveSmoothTime { get; private set; }
        
        [field: Header("Shooting")]
        [field: SerializeField] public GameObject BulletPrefab { get; private set; }
    }
}