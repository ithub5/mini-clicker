using UnityEngine;

namespace StateMachinePattern.PlayerSystem.Data
{
    [CreateAssetMenu(fileName = "NewPlayerViewSettings", menuName = "SOs/StateMachine/Player/ViewSettings")]
    public class PlayerViewSettingsSO : ScriptableObject
    {
        [field: SerializeField] [field: Range(0f, 1f)] public float Visibility { get; private set; }
    }
}