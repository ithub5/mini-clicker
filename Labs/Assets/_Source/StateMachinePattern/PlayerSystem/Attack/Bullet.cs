using System;
using System.Collections;
using UnityEngine;

namespace StateMachinePattern.PlayerSystem.Attack
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D rb;
        
        [SerializeField] private float travelVelocity;
        [SerializeField] private float lifeTime;

        private void OnEnable()
        {
            rb.velocity = transform.up * travelVelocity;
        }

        private IEnumerator SelfDestruction()
        {
            yield return new WaitForSeconds(lifeTime);
            
            Destroy(gameObject);
        }
    }
}