using System;
using System.Collections;
using System.Collections.Generic;
using Singleton.Utils.Enums;
using TMPro;
using UnityEngine;

namespace Singleton.ResourceSystem
{
    public class ResourceView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI woodAmountText;
        [SerializeField] private TextMeshProUGUI stoneAmountText;
        [SerializeField] private TextMeshProUGUI goldAmountText;

        private void OnEnable()
        {
            ResourceBank.Instance.OnResourceAmountChange += UpdateResourceAmountText;

            woodAmountText.text = ResourceBank.Instance.GetResourceAmount(ResourceType.Wood).ToString();
            stoneAmountText.text = ResourceBank.Instance.GetResourceAmount(ResourceType.Stone).ToString();
            goldAmountText.text = ResourceBank.Instance.GetResourceAmount(ResourceType.Gold).ToString();
        }

        private void OnDisable()
        {
            ResourceBank.Instance.OnResourceAmountChange -= UpdateResourceAmountText;
        }

        private void UpdateResourceAmountText(ResourceType resourceType, int resourceAmount)
        {
            switch (resourceType)
            {
                case ResourceType.Wood:
                    woodAmountText.text = resourceAmount.ToString();
                    break;
                case ResourceType.Stone:
                    stoneAmountText.text = resourceAmount.ToString();
                    break;
                case ResourceType.Gold:
                    goldAmountText.text = resourceAmount.ToString();
                    break;
            }
        }
    }
}
