using System;
using System.Collections;
using System.Collections.Generic;
using Singleton.ResourceSystem;
using Singleton.Utils.Enums;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Singleton.ResourceSystem
{
    public class ResourceManager : MonoBehaviour
    {
        [SerializeField] private TMP_InputField inputField;
        [SerializeField] private TMP_Dropdown dropdown;
        [SerializeField] private Button addBtn;

        private void OnEnable()
        {
            addBtn.onClick.AddListener(AddResource);
        }

        private void OnDisable()
        {
            addBtn.onClick.RemoveListener(AddResource);
        }

        private void AddResource()
        {
            ResourceType resourceType = (ResourceType)dropdown.value;
            int addAmount = int.Parse(inputField.text);

            ResourceBank.Instance.AddResource(resourceType, addAmount);
        }
    }
}
