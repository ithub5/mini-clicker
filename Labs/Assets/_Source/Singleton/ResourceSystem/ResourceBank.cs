using System;
using System.Collections;
using System.Collections.Generic;
using Singleton.ResourceSystem;
using Singleton.Utils.Delegates;
using Singleton.Utils.Enums;
using UnityEngine;

namespace Singleton.ResourceSystem
{
    public class ResourceBank
    {
        private static Lazy<ResourceBank> lazyInstance = new Lazy<ResourceBank>(() => new ResourceBank());
    
        public static ResourceBank Instance { get => lazyInstance.Value; }

        private Dictionary<ResourceType, Resource> _resources = new Dictionary<ResourceType, Resource>();

        public event ResourceHandler OnResourceAmountChange;

        public ResourceBank()
        {
            _resources.Add(ResourceType.Wood, new Resource(ResourceType.Wood, 10));
            _resources.Add(ResourceType.Stone, new Resource(ResourceType.Stone, 6));
            _resources.Add(ResourceType.Gold, new Resource(ResourceType.Gold, 3));

            int resourcesCount = Enum.GetValues(typeof(ResourceType)).Length;
            for (int resourceNum = 0; resourceNum < resourcesCount ; resourceNum++)
            {
                _resources[(ResourceType)resourceNum].OnAmountChange += 
                    (resourceType, amount) => OnResourceAmountChange?.Invoke(resourceType, amount);
            }
        }

        public int GetResourceAmount(ResourceType resourceType)
        {
            return _resources[resourceType].ResourceAmount;
        }
        
        public void AddResource(ResourceType resourceType, int addAmount)
        {
            _resources[resourceType].ResourceAmount += addAmount;
        }
    }
}
