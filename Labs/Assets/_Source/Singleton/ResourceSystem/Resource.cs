using System.Collections;
using System.Collections.Generic;
using Singleton.Utils.Delegates;
using Singleton.Utils.Enums;
using UnityEngine;

namespace Singleton.ResourceSystem
{
    public class Resource
    {
        public ResourceType resourceType { get; private set; }

        private int _resourceAmount;

        public int ResourceAmount
        {
            get => _resourceAmount;
            set
            {
                _resourceAmount = value;
                OnAmountChange?.Invoke(resourceType, _resourceAmount);
            }
        }

        public event ResourceHandler OnAmountChange;

        public Resource(ResourceType type, int startAmount)
        {
            resourceType = type;
            ResourceAmount = startAmount;
        }
        
    }
}
