using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Singleton.Utils.Enums
{
    public enum ResourceType
    {
        Wood, 
        Stone,
        Gold
    }
}