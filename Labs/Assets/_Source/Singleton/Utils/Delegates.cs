using System.Collections;
using System.Collections.Generic;
using Singleton.Utils.Enums;
using UnityEngine;

namespace Singleton.Utils.Delegates
{
    public delegate void ResourceHandler(ResourceType resourceType, int amount);
}