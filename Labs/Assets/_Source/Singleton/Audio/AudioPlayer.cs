using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Singleton.Audio
{
    public class AudioPlayer : MonoBehaviour
    {
        public static AudioPlayer Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = new AudioPlayer();
                DontDestroyOnLoad(gameObject);
                return;
            }
        
            Destroy(gameObject);
        }
    }
}


