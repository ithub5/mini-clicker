using UnityEngine;

namespace ObserverPattern.Data
{
    [CreateAssetMenu(fileName = "DaytimeSettings", menuName = "SOs/Observer/DayTimeSettings")]
    public class DaytimeSettingsSO : ScriptableObject
    {
        [field: SerializeField] public float DayLength { get; private set; }
        [field: SerializeField] public float UpdateFrequency { get; private set; }
        
        [field: Header("Sun Positions")]
        [field: SerializeField, Range(0f, 360f)] public float MorningSunPos { get; private set; }
        [field: SerializeField, Range(0f, 360f)] public float AfternoonSunPos { get; private set; }
        [field: SerializeField, Range(0f, 360f)] public float EveningSunPos { get; private set; }
        [field: SerializeField, Range(0f, 360f)] public float NightSunPos { get; private set; }
        
        [field: Header("Sky Colors")]
        [field: SerializeField] public Color MorningSkyColor { get; private set; }
        [field: SerializeField] public Color AfternoonSkyColor { get; private set; }
        [field: SerializeField] public Color EveningSkyColor { get; private set; }
        [field: SerializeField] public Color NightSkyColor { get; private set; }

        [field: Header("Stars Alpha")]
        [field: SerializeField, Range(0f, 1f)] public float MorningStartAlpha { get; private set; }
        [field: SerializeField, Range(0f, 1f)] public float AfternoonStartAlpha { get; private set; }
        [field: SerializeField, Range(0f, 1f)] public float EveningStartAlpha { get; private set; }
        [field: SerializeField, Range(0f, 1f)] public float NightStartAlpha { get; private set; }
    }
}