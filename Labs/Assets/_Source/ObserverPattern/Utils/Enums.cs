namespace ObserverPattern.Utils
{
    public enum Daytime
    {
        morning = 0,
        afternoon = 1,
        evening = 2,
        night = 3
    }
}