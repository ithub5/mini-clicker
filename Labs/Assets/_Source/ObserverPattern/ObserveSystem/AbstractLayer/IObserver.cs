namespace ObserverPattern.ObserveSystem.AbstractLayer
{
    public interface IObserver
    {
        public void Update();
    }
}