using System;
using ObserverPattern.ObserveSystem.AbstractLayer;
using ObserverPattern.Utils;

namespace ObserverPattern.ObserveSystem.DaytimeSystem.Observes
{
    public class Sun : IObserver
    {
        private TimeOfDay _time;

        public event Action<Daytime, float> OnUpdate;

        public Sun(TimeOfDay time)
        {
            _time = time;
            _time.AddObserver(this);
        }

        public void Update()
        {
            OnUpdate?.Invoke(_time.Daytime, _time.DaytimeProgress);
        }
    }
}