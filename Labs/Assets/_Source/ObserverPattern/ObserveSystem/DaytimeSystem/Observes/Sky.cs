using System;
using ObserverPattern.ObserveSystem.AbstractLayer;
using ObserverPattern.Utils;

namespace ObserverPattern.ObserveSystem.DaytimeSystem.Observes
{
    public class Sky : IObserver
    {
        private TimeOfDay _time;

        public event Action<Daytime, float> OnUpdate;

        public Sky(TimeOfDay time)
        {
            _time = time;
            _time.AddObserver(this);
        }
        
        public void Update()
        {
            OnUpdate?.Invoke(_time.Daytime, _time.DaytimeProgress);
        }
    }
}