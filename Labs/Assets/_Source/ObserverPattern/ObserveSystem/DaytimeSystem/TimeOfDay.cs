using System;
using System.Collections.Generic;
using DG.Tweening;
using ObserverPattern.ObserveSystem.AbstractLayer;
using ObserverPattern.Utils;

namespace ObserverPattern.ObserveSystem.DaytimeSystem
{
    public class TimeOfDay : IObservable
    {
        private List<IObserver> _observers;

        private Sequence _dayCounterSequence;
        
        private float _dayLenght;
        private float _dayQuarterLength;
        private float _updateFrequency;
        private float _daytimeCounter;
        
        public Daytime Daytime { get; private set; }
        public float DaytimeProgress { get; private set; }

        public TimeOfDay(float dayLength, float updateFrequency)
        {
            _observers = new List<IObserver>();

            _dayLenght = dayLength;
            _dayQuarterLength = dayLength / 4;
            _updateFrequency = updateFrequency;
            InitSequence();
        }

        public void AddObserver(IObserver o)
        {
            if (_observers.Contains(o))
            {
                return;
            }

            _observers.Add(o);
        }

        public void RemoveObserver(IObserver o)
        {
            _observers.Remove(o);
        }

        public void Notify()
        {
            Daytime = (Daytime)(MathF.Floor(_daytimeCounter / _dayQuarterLength));
            DaytimeProgress = _daytimeCounter % _dayQuarterLength;
            
            foreach (var o in _observers)
            {
                o.Update();
            }
        }

        private void InitSequence()
        {
            _dayCounterSequence = DOTween.Sequence();
            _dayCounterSequence.SetAutoKill(false);
            
            _dayCounterSequence.AppendCallback(Notify);
            _dayCounterSequence.AppendInterval(_updateFrequency);
            _dayCounterSequence.AppendCallback(() => _daytimeCounter += _updateFrequency);
            _dayCounterSequence.AppendCallback(() => { if (_daytimeCounter >= _dayLenght) _daytimeCounter = 0; });
            _dayCounterSequence.AppendCallback(() => _dayCounterSequence.Restart());
        }
    }
}