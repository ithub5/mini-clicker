using System;
using ObserverPattern.ObserveSystem.AbstractLayer;
using ObserverPattern.ObserveSystem.DaytimeSystem.Observes;
using ObserverPattern.Utils;
using UnityEngine;

namespace ObserverPattern.ObserveSystem.DaytimeSystem.Views
{
    public class SunView : MonoBehaviour
    {
        [SerializeField] private Transform target;
        
        private float[] _rotations;

        private Sun _observer;
        
        public void Init(Sun o, params float[] rotations)
        {
            _observer = o;
            _rotations = rotations;
            
            _observer.OnUpdate += Rotate;
        }

        private void Rotate(Daytime daytime, float daytimeProgress)
        {
            int daytimeInt = (int)daytime;
            int nextIndex = daytimeInt + 1 == _rotations.Length ? 0 : daytimeInt + 1;
            float zRotation = Mathf.LerpAngle(_rotations[daytimeInt], _rotations[nextIndex], daytimeProgress);

            Vector3 targetRotation = target.eulerAngles;
            target.eulerAngles = new Vector3(targetRotation.x, targetRotation.y, zRotation);
        }
    }
}