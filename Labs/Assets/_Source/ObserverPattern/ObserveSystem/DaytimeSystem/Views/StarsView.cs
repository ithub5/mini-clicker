using ObserverPattern.ObserveSystem.DaytimeSystem.Observes;
using ObserverPattern.Utils;
using UnityEngine;

namespace ObserverPattern.ObserveSystem.DaytimeSystem.Views
{
    public class StarsView : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer[] starsRenderers;
        
        private float[] _alphas;

        private Stars _observer;
        
        public void Init(Stars o, params float[] alphas)
        {
            _observer = o;
            _alphas = alphas;
            
            _observer.OnUpdate += Fade;
        }

        private void Fade(Daytime daytime, float daytimeProgress)
        {
            int daytimeInt = (int)daytime;
            int nextIndex = daytimeInt + 1 == _alphas.Length ? 0 : daytimeInt + 1;
            float alpha = Mathf.Lerp(_alphas[daytimeInt], _alphas[nextIndex], daytimeProgress);

            foreach (var renderer in starsRenderers)
            {
                renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, alpha);
            }
        }
    }
}