using ObserverPattern.ObserveSystem.DaytimeSystem.Observes;
using ObserverPattern.Utils;
using UnityEngine;

namespace ObserverPattern.ObserveSystem.DaytimeSystem.Views
{
    public class SkyView : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer spriteRenderer;
        
        private Color[] _colors;

        private Sky _observer;
        
        public void Init(Sky o, params Color[] colors)
        {
            _observer = o;
            _colors = colors;
            
            _observer.OnUpdate += Fade;
        }

        private void Fade(Daytime daytime, float daytimeProgress)
        {
            int daytimeInt = (int)daytime;
            int nextIndex = daytimeInt + 1 == _colors.Length ? 0 : daytimeInt + 1;
            Color fadedColor = Color.Lerp(_colors[daytimeInt], _colors[nextIndex], daytimeProgress);

            spriteRenderer.color = fadedColor;
        }
    }
}