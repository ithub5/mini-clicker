using ObserverPattern.Data;
using ObserverPattern.ObserveSystem.DaytimeSystem;
using ObserverPattern.ObserveSystem.DaytimeSystem.Observes;
using ObserverPattern.ObserveSystem.DaytimeSystem.Views;
using UnityEngine;
using UnityEngine.Rendering;

namespace ObserverPattern.Core
{
    public class Bootstrapper : MonoBehaviour
    {
        [SerializeField] private DaytimeSettingsSO _daytimeSettingsSO;

        [SerializeField] private SunView sunView;
        [SerializeField] private SkyView skyView;
        [SerializeField] private StarsView starsView;

        private void Awake()
        {
            TimeOfDay timeOfDay = new TimeOfDay(_daytimeSettingsSO.DayLength, _daytimeSettingsSO.UpdateFrequency);
            
            sunView.Init(new Sun(timeOfDay), _daytimeSettingsSO.MorningSunPos, _daytimeSettingsSO.AfternoonSunPos,
                _daytimeSettingsSO.EveningSunPos, _daytimeSettingsSO.NightSunPos);
            
            skyView.Init(new Sky(timeOfDay), _daytimeSettingsSO.MorningSkyColor, _daytimeSettingsSO.AfternoonSkyColor,
                _daytimeSettingsSO.EveningSkyColor, _daytimeSettingsSO.NightSkyColor);
            
            starsView.Init(new Stars(timeOfDay), _daytimeSettingsSO.MorningStartAlpha, _daytimeSettingsSO.AfternoonStartAlpha,
                _daytimeSettingsSO.EveningStartAlpha, _daytimeSettingsSO.NightStartAlpha);
        }
    }
}