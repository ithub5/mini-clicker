using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motor : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;
    [SerializeField] private float forceAmount;

    private bool _isOnRight;
    private float _forceToApply;
    
    
    private void Start()
    {
        _forceToApply = forceAmount;
        
        rb.velocity = transform.right * _forceToApply;
    }

    private void FixedUpdate()
    {
        if ((transform.position.x < 0 && !_isOnRight) || transform.position.x > 0 && _isOnRight)
        {
            Debug.Log("apply");
            _forceToApply *= -1;
            rb.velocity = transform.right * _forceToApply;

            _isOnRight = !_isOnRight;
        }
    }
}
