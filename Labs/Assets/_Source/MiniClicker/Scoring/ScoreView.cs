using System;
using System.Collections;
using System.Collections.Generic;
using Scoring;
using TMPro;
using UnityEngine;

namespace Scoring
{
    public class ScoreView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI textUI;

        private Score _score;

        public void Constructor(Score score)
        {
            _score = score;
            _score.OnPlayerScoreChange += UpdateText;
        }

        private void UpdateText()
        {
            textUI.text = _score.PlayerScore.ToString();
        }
    }
}
