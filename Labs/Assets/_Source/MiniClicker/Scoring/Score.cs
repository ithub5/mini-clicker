using Utils.Delegates;

namespace Scoring
{
    public class Score
    {
        private int _playerScore;
        
        public int PlayerScore 
        { 
            get => _playerScore;
            set
            {
                _playerScore = value;
                OnPlayerScoreChange?.Invoke();
            }
        }
        public VoidHandler OnPlayerScoreChange;
    }
}