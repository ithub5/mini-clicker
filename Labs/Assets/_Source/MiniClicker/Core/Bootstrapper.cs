using System;
using System.Collections;
using System.Collections.Generic;
using PlayerInput;
using Scoring;
using UnityEngine;

namespace Core
{
    public class Bootstrapper : MonoBehaviour
    {
        [SerializeField] private InputHandler input;
        [SerializeField] private InputOverObject[] inputsOverObject;

        [SerializeField] private ScoreView scoreView;
        
        private Game _game;
        private Score _score;
        
        private void Awake()
        {
            _score = new Score();
            scoreView.Constructor(_score);
            for (int objectIndx = 0; objectIndx < inputsOverObject.Length; objectIndx++)
            {
                inputsOverObject[objectIndx].Constructor(_score);
            }
            
            _game = new Game(_score, input);
        }
    }
}
