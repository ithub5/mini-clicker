using System.Collections;
using System.Collections.Generic;
using PlayerInput;
using Scoring;
using UnityEngine;

namespace Core
{
    public class Game
    {
        private Score _score;
        private InputHandler _input;

        private const int START_SCORE = 5;
        
        public Game(Score score, InputHandler input)
        {
            _score = score;
            _input = input;
            
            Start();
        }
    
        private void Start()
        {
            _score.PlayerScore = START_SCORE;

            _input.OnQuitKeyPressed += Quit;
        }

        private void Quit()
        {
            _score.PlayerScore = 0;

            _input.OnQuitKeyPressed -= Quit;
        }
    }
}

