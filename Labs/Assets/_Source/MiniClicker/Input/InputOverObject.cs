using System;
using System.Collections;
using System.Collections.Generic;
using Scoring;
using UnityEngine;

namespace PlayerInput
{
    public class InputOverObject : MonoBehaviour
    {
        [SerializeField] [Tooltip("can be negative")]
        private int scoreToAdd;
    
        private Score _score;

        public void Constructor(Score score)
        {
            _score = score;
        }
    
        private void OnMouseDown()
        {
            _score.PlayerScore += scoreToAdd;
        }
    }
}
