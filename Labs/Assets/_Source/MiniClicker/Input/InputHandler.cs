using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils.Delegates;

namespace PlayerInput
{
    public class InputHandler : MonoBehaviour
    {
        private const KeyCode QUIT_GAME_KEY = KeyCode.Escape;

        public event VoidHandler OnQuitKeyPressed;

        private void Update()
        {
            if (Input.GetKeyDown(QUIT_GAME_KEY))
            {
                OnQuitKeyPressed?.Invoke();
            }
        }
    }
}

